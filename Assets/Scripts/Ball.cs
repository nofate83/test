﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    
    void OnCollisionEnter2D(Collision2D other) 
    {
        UI.Instance.BallHit();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Planet", menuName = "Add Planet", order = 51)]
public class PlanetData : ScriptableObject
{
    [SerializeField]
    private string _planetName;
    [SerializeField]
    private float _gravity;
    [SerializeField]
    private Color _planetColor;
    [SerializeField]
    
    

    public string planetName
    {
        get { return _planetName; }
    }

    public float gravity
    {
        get { return _gravity; }
    }
    public Color planetColor
    {
        get { return _planetColor; }
    }

   
}

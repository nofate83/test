﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour, IClickable
{
    [SerializeField]
    SpriteRenderer sr;
    public void Hited()
    {
        ChangeColorRandomly();
    }

    public void Touched()
    {
        ChangeColorRandomly();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Hited();
    }

    void ChangeColorRandomly()
    {
        sr.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetController : MonoBehaviour
{
    static PlanetController _instance;
          
    [Header("Planet Objects")]
    [SerializeField]
    Rigidbody2D ballRb;
    [SerializeField]
    float forceMultiplier;
    

    string planetName;
       
    public static PlanetController Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.Find("Planet").GetComponent<PlanetController>();
            return _instance;
        }
    }
    
    public void InitPlanet(PlanetData planetData)
    {
        planetName = planetData.planetName;
        Physics2D.gravity = Vector2.down * planetData.gravity;
        Camera.main.backgroundColor = planetData.planetColor;
        Time.timeScale = 1;
    }

    void Update()
    {
        if (Application.platform == (RuntimePlatform.WindowsEditor | RuntimePlatform.WindowsPlayer))
        {
            if (Input.GetMouseButton(0) && !UI.Instance.menu.isActiveAndEnabled)
                ballRb.AddForce(Camera.main.ScreenToWorldPoint(Input.mousePosition) * forceMultiplier, ForceMode2D.Force);
            
            if (Input.GetKeyDown(KeyCode.Backspace))
                UI.Instance.OpenMenu();
        }
        if (Application.platform == (RuntimePlatform.Android | RuntimePlatform.IPhonePlayer) && Input.touchCount > 0)
            ballRb.AddForce(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position) * forceMultiplier);
    }
}

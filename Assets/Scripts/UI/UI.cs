﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    static UI _instance;

    [SerializeField]
    public Menu menu;
    [SerializeField]
    Text hitsCountUILabel;
    [SerializeField]
    Button backBtn;

    int hitsCount;

    public static UI Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.Find("UI").GetComponent<UI>();
            return _instance;
        }
    }

    void Start()
    {
        backBtn.onClick.AddListener(OpenMenu);
    }

    public void BallHit()
    {
        hitsCountUILabel.text = "Ball Hits: " + ++hitsCount;
    }

    public void OpenMenu()
    {
        menu.gameObject.SetActive(true);
    }
}

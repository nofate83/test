﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
       
    [SerializeField]
    Button earthBtn;
    [SerializeField]
    Button moonBtn;
    [SerializeField]
    Button jupiterBtn;

    [Header("Planet data")]
    [SerializeField]
    public PlanetData earth;
    [SerializeField]
    public PlanetData moon;
    [SerializeField]
    public PlanetData jupiter;


    void Start()
    {
        earthBtn.onClick.AddListener(() => InitPlanet(earth));      //we can add listener from editor, but i think this way more comfortable to edit 
        moonBtn.onClick.AddListener(() => InitPlanet(moon));
        jupiterBtn.onClick.AddListener(() => InitPlanet(jupiter));
    }

    void InitPlanet(PlanetData planetData)
    {
        PlanetController.Instance.InitPlanet(planetData);
        gameObject.SetActive(false);
    }
           

    void OnEnable()
    {
        Time.timeScale = 0;
    }
}
